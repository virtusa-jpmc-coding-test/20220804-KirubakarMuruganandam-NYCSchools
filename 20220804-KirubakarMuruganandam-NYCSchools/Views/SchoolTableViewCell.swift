//
//  SchoolTableViewCell.swift
//  20220804-KirubakarMuruganandam-NYCSchools
//
//  Created by Kirubakar Muruganandam on 8/4/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    static let identifier = "SchoolTableViewCell"
    
    @IBOutlet var cardView: UIView!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var navigateButton: UIButton!

    var school: School! {
        didSet {
            schoolNameLabel.text = school.school_name
            if let city = school.city, let code = school.state_code, let zip = school.zip{
                cityLabel.text = "\(city), \(code), \(zip)"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCardViewShadows()
        
        self.navigateButton.layer.cornerRadius = 15
    }
    
    func setupCardViewShadows(){
        let view = cardView
        view?.layer.cornerRadius = 15.0
        view?.layer.shadowColor = UIColor.black.cgColor
        view?.layer.shadowOffset = CGSize(width: 0, height: 2)
        view?.layer.shadowOpacity = 0.8
        view?.layer.shadowRadius = 3
        view?.layer.masksToBounds = false
    }
}
