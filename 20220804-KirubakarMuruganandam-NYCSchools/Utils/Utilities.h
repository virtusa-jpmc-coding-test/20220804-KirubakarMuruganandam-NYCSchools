//
//  Utilities.h
//  20220804-KirubakarMuruganandam-NYCSchools
//
//  Created by Kirubakar Muruganandam on 8/4/22.
//

#ifndef Utilities_h
#define Utilities_h


#endif /* Utilities_h */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface Utilities : NSObject {
}

+ (UIAlertController *)getAlertController:(NSString *)message;
@end
