//
//  School.swift
//  20220804-KirubakarMuruganandam-NYCSchools
//
//  Created by Kirubakar Muruganandam on 8/4/22.
//

import Foundation
struct School: Codable {
    let dbn: String?
    let school_name: String?
    let overview_paragraph: String?
    let location: String?
    let phone_number: String?
    let fax_number: String?
    let school_email: String?
    let website: String?
    let total_students: String?
    let primary_address_line_1: String?
    let city: String?
    let zip: String?
    let state_code: String?
    let latitude: String?
    let longitude: String?
    
    var satScores: SchoolSATScores? //sets the SAT Scores object
}

struct SchoolSATScores: Codable {
    let dbn: String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}

