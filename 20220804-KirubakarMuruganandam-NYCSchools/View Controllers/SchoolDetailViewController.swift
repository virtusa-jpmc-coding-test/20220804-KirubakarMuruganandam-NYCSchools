//
//  SchoolDetailViewController.swift
//  20220804-KirubakarMuruganandam-NYCSchools
//
//  Created by Kirubakar Muruganandam on 8/4/22.
//

import UIKit
import MapKit

class SchoolDetailViewController: UIViewController {
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var readingSATScoreLabel: UILabel!
    @IBOutlet weak var mathSATScoreLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    
    @IBOutlet weak var overviewLabel: UILabel!
    
    @IBOutlet weak var addressLineLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!


    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loadDetailView(_ school: School) {
                
        self.title = school.school_name
        
        if let readingScore = school.satScores?.sat_critical_reading_avg_score {
            readingSATScoreLabel.text = "SAT Average Reading Score - " + readingScore
        }
        
        if let writingScore = school.satScores?.sat_writing_avg_score {
            writingLabel.text = "SAT Average Writing Score - " + writingScore
        }
        
        if let mathsScore = school.satScores?.sat_math_avg_score {
            mathSATScoreLabel.text = "SAT Average Maths Score - " + mathsScore
        }
        
        if let overview = school.overview_paragraph {
            overviewLabel.text = overview
        }
        
        if let schoolAddress = school.location{
            let address = schoolAddress.components(separatedBy: "(")
            addressLineLabel.text = address[0]
        }
        
        websiteLabel.text = school.website
        
        if let phoneNumber = school.phone_number {
            phoneNumberLabel.text = "Phone:  " + phoneNumber
        }
        
        if let email = school.school_email {
            emailLabel.text = "Email:  " + email
        }
    }
}
